# speaq

<img src="https://i.imgur.com/AqPZyXR.png" alt="the speaq logo">

A modern chat app built with <b>Electron</b>, <b>Vue.js</b> and <b>Express</b>.

## Client

```
cd client/
```

### Install dependencies

```
yarn
```

### Compiles and hot-reloads for development

```
yarn electron:serve
```

### Compiles and minifies for production

```
yarn electron:build
```

## Server

```
cd server/
```

### Install dependencies

```
yarn
```

### Run

```
yarn start
```

## Socket server

```
cd socket/
```

### Run

```
yarn start
```
