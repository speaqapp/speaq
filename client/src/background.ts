// ───── Imports ───────────────────────────────────

import {
  app,
  protocol,
  BrowserWindow,
  ipcMain,
  Notification,
  Tray,
  Menu,
  nativeImage,
  net,
} from 'electron';
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib';
import installExtension, { VUEJS3_DEVTOOLS } from 'electron-devtools-installer';
import path from 'path';

const isDevelopment = process.env.NODE_ENV !== 'production';

// ───── Registers scheme ──────────────────────────

protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } },
]);

// ───── Create the main window ────────────────────

let win: BrowserWindow;

async function createWindow() {
  win = new BrowserWindow({
    height: 600,
    width: 900,
    minHeight: 500,
    minWidth: 800,
    frame: false,
    show: false,
    backgroundColor: '#141414',
    webPreferences: {
      nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION as unknown as boolean,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION,
      enableRemoteModule: true,
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  // ───── Loads the url of the dev server if in development ──────────
  // ───── Loads the index.html when not in development ───────────────

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
  } else {
    createProtocol('app');
    win.loadURL('app://./index.html');
  }
}

// ───── Create the login window ────────────────────

let loginWin: BrowserWindow;

async function createLoginWindow() {
  loginWin = new BrowserWindow({
    height: 500,
    maxHeight: 500,
    maxWidth: 1000,
    minHeight: 500,
    minWidth: 1000,
    width: 1000,
    frame: false,
    show: false,
    maximizable: false,
    backgroundColor: '#EFEFEF',
    webPreferences: {
      nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION as unknown as boolean,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION,
      enableRemoteModule: true,
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  // ───── Loads the url of the dev server if in development ──────────
  // ───── Loads the index.html when not in development ───────────────

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    await loginWin.loadURL((process.env.WEBPACK_DEV_SERVER_URL + '/#/login') as string);
  } else {
    loginWin.loadURL('app://./index.html#login');
  }
}

let isLoggedIn = false;

// ───── Installs Vue Devtools ──────────────────────
// ─────  & Starts windows for first time ───────────

app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    try {
      await installExtension(VUEJS3_DEVTOOLS);
    } catch (e: any) {
      console.error('Vue Devtools failed to install:', e.toString());
    }
  }

  // ───── Logged in check ────────────────────────────
  // Creates hidden window, checks if user is logged in in renderer
  // Shows win if they are, creates loginWin if not

  // createWindow().then(() => {
  //   win.show();
  // });

  createWindow().then(() => {
    // win.once('ready-to-show', () => {
    if (isLoggedIn) {
      win.maximize();
      win.show();
    } else {
      win.close();
      createLoginWindow().then(() => loginWin.show());
    }
    // });
  });
});

// ───── Exit cleanly in dev ────────────────────────

if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit();
      }
    });
  } else {
    process.on('SIGTERM', () => {
      app.quit();
    });
  }
}

// ───── Checks if all windows ──────────────────────
// ───── are closed and quits app ───────────────────

// app.on('window-all-closed', () => {
//   if (process.platform !== 'darwin') {
//     app.quit();
//   }
// });

// ───── IPC events ─────────────────────────────────

ipcMain.on('closeWindow', () => {
  win.hide();
});

ipcMain.on('maximizeWindow', () => {
  win.isMaximized() ? win.unmaximize() : win.maximize();
});

ipcMain.on('minimizeWindow', () => {
  win.minimize();
});

ipcMain.on('loginCheck', (e, user) => {
  if (user) {
    isLoggedIn = true;
  } else {
    isLoggedIn = false;
  }
});

ipcMain.on('logout', () => {
  isLoggedIn = false;
  win.close();
  createLoginWindow().then(() => {
    loginWin.show();
  });
});

ipcMain.on('login', () => {
  isLoggedIn = true;
  loginWin.close();
  createWindow().then(() => {
    win.maximize();
    win.show();
  });
});
