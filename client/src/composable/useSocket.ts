/* eslint-disable import/prefer-default-export */

import { io } from 'socket.io-client';
import { ref } from 'vue';

import { useUserStore } from '@/store/user';

export function useSocket() {
  const socket = io('ws://localhost:8900');

  const userStore = useUserStore();

  function addUser() {
    socket.emit('addUser', userStore.user?.id);
    console.log(`added user: ${userStore.user?.id}`);
  }

  function sendMessage(senderId: number | undefined, receiverId: number | undefined, text: string) {
    socket.emit('sendMessage', { senderId, receiverId, text });
  }

  const message = ref({ senderId: '', text: '' });

  socket.on('getMessage', ({ senderId, text }) => {
    message.value = { senderId, text };
    console.log(message.value);
  });

  return {
    addUser,
    sendMessage,
  };
}
