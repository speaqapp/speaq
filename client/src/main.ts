import { createPinia } from 'pinia';
import axios from 'axios';
import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './styles/styles.css';

axios.defaults.baseURL = 'http://localhost:8081';

const pinia = createPinia();
const app = createApp(App).use(pinia).use(router);
app.mount('#app');
