import { contextBridge, ipcRenderer } from 'electron';

// ───── Defines valid IPC Channels ────────────────

const validChannels = ['closeWindow', 'minimizeWindow', 'maximizeWindow', 'logout', 'loginCheck', 'login'];

// ───── Exposes ipcRenderer to client ─────────────

contextBridge.exposeInMainWorld('ipcRenderer', {
  send: ( channel, data ) => {
    if ( validChannels.includes( channel ) ) {
      ipcRenderer.send( channel, data );
    }
  },
  receive: ( channel, func ) => {
    if ( validChannels.includes( channel ) ) {
      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, ( event, ...args ) => func(...args));
    }
  },
});
