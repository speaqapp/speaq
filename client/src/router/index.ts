import {
  createRouter, createWebHistory, RouteRecordRaw, createWebHashHistory,
} from 'vue-router';
import Home from '../views/Home/Home.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        name: 'Conversations',
        path: '',
        component: () => import('../views/Home/Chat/Conversations.vue'),
        children: [
          {
            name: 'User',
            path: '',
            props: true,
            component: () => import('../views/Home/Chat/User.vue'),
          },
        ],
      },
      {
        name: 'Friends',
        path: '',
        component: () => import('../views/Home/Friends.vue'),
      },
    ],
  },
  {
    path: '/home',
    redirect: '/',
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/404',
    name: 'NotFound',
    component: () => import('../views/NotFound.vue'),
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/404',
  },
];

const router = createRouter({
  history: process.env.IS_ELECTRON ? createWebHashHistory() : createWebHistory(),
  routes,
});

export default router;
