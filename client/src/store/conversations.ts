/* eslint-disable import/prefer-default-export */

import { defineStore } from 'pinia';
import { ref } from 'vue';

// ───── Defining Conversation type ─────────────────

interface Conversation {
  id: number;
  members: string[];
  createdAt: string;
  updatedAt: string;
}

// ───── Defining conversations store ───────────────

export const useConversationsStore = defineStore('conversations', () => {
  const pinnedConversations = ref([] as Conversation[]);
  const lastConversation = ref(null as number | null);
  function pinConversation(payload: any) {
    if (!pinnedConversations.value.includes(payload)) pinnedConversations.value.push(payload);
  }
  function unpinConversation(payload: number) {
    pinnedConversations.value.splice(payload, 1);
  }
  function clearPinnedConversations() {
    pinnedConversations.value = [];
  }
  function changeLastConversation(payload: number) {
    lastConversation.value = payload;
  }

  return {
    pinnedConversations,
    lastConversation,
    pinConversation,
    unpinConversation,
    clearPinnedConversations,
    changeLastConversation,
  };
});
