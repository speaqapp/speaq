/* eslint-disable import/prefer-default-export */

import { defineStore } from 'pinia';
import { ref } from 'vue';

// ───── Defining misc store ────────────────────────

export const useMiscStore = defineStore('misc', () => {
  const emojiPickerOpen = ref(false);

  function toggleEmojiPickerOpen() {
    emojiPickerOpen.value = !emojiPickerOpen.value;
  }

  return { emojiPickerOpen, toggleEmojiPickerOpen };
});
