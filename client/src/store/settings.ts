/* eslint-disable import/prefer-default-export */

import { defineStore } from 'pinia';
import { ref, watch } from 'vue';

// ───── Defining settings store ────────────────────

export const useSettingsStore = defineStore('settings', () => {
  const darkTheme = ref(false);

  function toggleTheme() {
    darkTheme.value = !darkTheme.value;
  }

  if (localStorage.getItem('darkTheme')) {
    darkTheme.value = JSON.parse(localStorage.getItem('darkTheme')!);
  }

  watch(
    darkTheme,
    () => {
      localStorage.setItem('darkTheme', JSON.stringify(darkTheme.value));
    },
    { deep: true },
  );

  return { darkTheme, toggleTheme };
});
