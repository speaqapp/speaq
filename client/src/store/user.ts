/* eslint-disable import/prefer-default-export */

import { defineStore } from 'pinia';
import { useConversationsStore } from '@/store/conversations';
import { ref, watch } from 'vue';

// ───── Defining User type ─────────────────────────

interface User {
  id: number;
  username: string;
  email: string;
  profilePicture: string | null;
}

// ───── Defining user store ────────────────────────

export const useUserStore = defineStore('user', () => {
  const user = ref(null as User | null);
  const conversations = useConversationsStore();

  function login(payload: User) {
    user.value = payload;
  }
  function logout() {
    user.value = null;
    conversations.lastConversation = null;
    conversations.pinnedConversations = [];
    localStorage.clear();
  }

  if (localStorage.getItem('user')) {
    user.value = JSON.parse(localStorage.getItem('user')!);
  }

  watch(
    user,
    () => {
      localStorage.setItem('user', JSON.stringify(user.value));
    },
    { deep: true },
  );

  return { user, login, logout };
});
