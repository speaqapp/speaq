module.exports = {
  pluginOptions: {
    electronBuilder: {
      preload: 'src/preload.js',
      builderOptions: {
        appId: 'com.franciscosilva.speaq',
        linux: {
          target: ['AppImage', 'deb', 'snap'],
          category: 'Chat',
        },
      },
    },
  },
};
