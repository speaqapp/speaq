import "reflect-metadata";
import express from "express";
import helmet from "helmet";
import cors from "cors";
import morgan from "morgan";

import { connection } from "./sequelize/connection";
import { routes } from "./routes";
import { config } from "./config";

const app = express();

app.use(helmet());
app.use(cors({ origin: "*" }));
app.use(morgan("short"));
app.use(express.json());

routes(app);

const start = async () => {
  try {
    await connection.sync({ force: false });
    app.listen(config.port, () => {
      console.log(`💫 listening on port ${config.port}`);
    });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

start();
