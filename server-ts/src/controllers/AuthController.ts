import { User } from "../models/User";
import { sign } from "jsonwebtoken";
import { config } from "../config/";
import { Request, Response } from "express";

function jwtSignUser(user: User) {
  const ONE_WEEK = 60 * 60 * 24 * 7;
  return sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_WEEK,
  });
}

export default {
  async register(req: Request, res: Response) {
    try {
      const user = await User.create(req.body);
      const userJSON = user.toJSON();
      res.send({ user: userJSON, token: jwtSignUser(userJSON) });
    } catch (err) {
      res.status(400).send({
        error: "This email is already in use",
      });
    }
  },
  async login(req: Request, res: Response) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ where: { email } });
      if (!user) {
        return res.status(403).send({ error: "Incorrect credentials." });
      }

      const isPasswordValid = await user.comparePassword(password);

      if (!isPasswordValid) {
        return res.status(403).send({ error: "Incorrect credentials." });
      }

      const userJSON = user.toJSON();
      res.send({ user: userJSON, token: jwtSignUser(userJSON) });
    } catch (err) {
      res.status(500).send({
        error: "Error signing in.",
      });
    }
  },
};
