import { Conversation } from "../models/Conversation";
import { Op } from "sequelize";
import { Request, Response } from "express";

export default {
  async newConversation(req: Request, res: Response) {
    const newConversation = Conversation.create(req.body);
    try {
      res.status(200).json(newConversation);
    } catch (err) {
      res.status(500).json(err);
    }
  },

  async getConversations(req: Request, res: Response) {
    try {
      const conversations = await Conversation.findAll({
        where: { membersId: { [Op.contains]: [req.params.id] } },
      });

      res.status(200).json(conversations);
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
