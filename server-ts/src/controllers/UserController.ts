import { User } from "../models/User";
import { Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { config } from "../config/";

export default {
  async changeProfilePicture(req: Request, res: Response) {
    const token = req.header("bearer");

    try {
      if (token) verify(token, config.authentication.jwtSecret);
    } catch (err) {
      return res.status(401).json("NOT AUTHORIZED");
    }

    try {
      const user = await User.update(
        { profilepicture: req.body.profilepicture },
        { where: { id: req.params.id } }
      );
      res.status(200).json(user);
    } catch (err) {
      res.status(500).json(err);
    }
  },
  async getUser(req: Request, res: Response) {
    try {
      const user = await User.findOne({ where: { id: req.params.id } });

      if (user) {
        const userData = {
          username: user.username,
          profilepicture: user.profilepicture,
        };
        res.status(200).json(userData);
      } else {
        res.status(400).json({ message: "user not found" });
      }
    } catch (err) {
      res.status(500).json(err);
    }
  },
  // async addFriend(req: Request, res: Response) {
  //   try {
  //     const { id, friendUsername } = req.body;

  //     const friend = await User.findOne({
  //       where: { username: friendUsername },
  //     });

  //     if (friend) {
  //       await User.update(
  //         {
  //           friends: sequelize.fn(
  //             "array_append",
  //             sequelize.col("friends"),
  //             friend.id
  //           ),
  //         },
  //         { where: { id: id } }
  //       );

  //       res.status(200).json();
  //     } else {
  //       res.status(400).json({ message: "user not found" });
  //     }
  //   } catch (err) {
  //     res.status(500).json(err);
  //   }
  // },
  // async getFriends(req: Request, res: Response) {
  //   try {
  //     const user = await User.findOne({ where: { id: req.params.id } });
  //     if (user) {
  //       const friends = user.friends;

  //     }
  //     res.status(200).json(friends);
  //   } catch (err) {
  //     res.status(500).json(err);
  //   }
  // },
};
