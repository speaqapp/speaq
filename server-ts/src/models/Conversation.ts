import {
  Table,
  Model,
  BelongsToMany,
  HasMany,
  Column,
  DataType,
  ForeignKey,
} from "sequelize-typescript";
import { ConversationUser } from "./ConversationUser";
import { Message } from "./Message";
import { User } from "./User";

@Table({
  timestamps: true,
  tableName: "conversations",
})
export class Conversation extends Model {
  @Column(DataType.ARRAY(DataType.STRING))
  declare membersId: string[];

  @BelongsToMany(() => User, () => ConversationUser)
  declare users: User[];

  @HasMany(() => Message)
  declare messages: Message[];
}
