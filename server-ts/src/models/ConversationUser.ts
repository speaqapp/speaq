import {
  Table,
  Model,
  ForeignKey,
  Column,
} from "sequelize-typescript";
import { Conversation } from "./Conversation";
import { User } from "./User";

@Table({
  timestamps: true,
  tableName: "conversationUser",
})
export class ConversationUser extends Model {
  @ForeignKey(() => Conversation)
  @Column
  declare conversationId: number;

  @ForeignKey(() => User)
  @Column
  declare userId: number;
}
