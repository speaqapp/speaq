import {
  Table,
  Model,
  Column,
  ForeignKey,
  BelongsTo,
} from "sequelize-typescript";
import { Conversation } from "./Conversation";
import { User } from "./User";

@Table({
  timestamps: true,
  tableName: "messages",
})
export class Message extends Model {
  @ForeignKey(() => User)
  @Column
  declare UserId: number;

  @BelongsTo(() => User)
  declare user: User;

  @ForeignKey(() => Conversation)
  @Column
  declare conversationId: number;

  @BelongsTo(() => Conversation)
  declare conversation: Conversation;
}
