import {
  Table,
  Model,
  Column,
  DataType,
  BeforeSave,
  HasMany,
  BelongsToMany,
} from "sequelize-typescript";
import bcrypt, { genSalt } from "bcrypt";
import { Conversation } from "./Conversation";
import { Message } from "./Message";
import { ConversationUser } from "./ConversationUser";

@Table({
  timestamps: true,
  tableName: "users",
})
export class User extends Model {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  declare username: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: true,
  })
  declare email: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  declare password: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  declare profilepicture: string;

  @BelongsToMany(() => Conversation, () => ConversationUser)
  declare conversations: Conversation[];

  @HasMany(() => Message)
  declare message: Message[];

  @BeforeSave
  static async hashPassword(user: User) {
    user.setDataValue(
      "password",
      await bcrypt.hash(user.getDataValue("password"), await genSalt(7))
    );
  }

  comparePassword = async (password: string) => {
    return await bcrypt.compare(password, this.password);
  };
}
