import { NextFunction, Request, Response } from "express";
import Joi from "joi";

export default {
  register(req: Request, res: Response, next: NextFunction) {
    const schema = Joi.object({
      email: Joi.string().min(6).required().email(),
      username: Joi.string().min(4).required(),
      password: Joi.string().min(6).required(),
    });

    const { error } = schema.validate(req.body);

    if (error) {
      switch (error.details[0].context?.key) {
        case "email":
          res.status(400).send({ error: "Invalid email" });
          break;
        case "username":
          res.status(400).send({ error: "Invalid username" });
          break;
        case "password":
          res.status(400).send({ error: "Invalid password" });
          break;
        default:
          res.status(400).send({ error: "invalid credentials" });
      }
    } else {
      next();
    }
  },
};
