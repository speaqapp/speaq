import { Application } from "express";

import AuthControllerPolicy from "./policies/AuthControllerPolicy";
import AuthController from "./controllers/AuthController";
import UserController from "./controllers/UserController";
import ConversationController from "./controllers/ConversationController";

export const routes = (app: Application) => {
  // Register an user
  app.post("/register", AuthControllerPolicy.register, AuthController.register);
  // Login an user
  app.post("/login", AuthController.login);
  // Get username and profile picture by id
  app.get("/user/:id", UserController.getUser);
  // Update user's profile picture
  app.put("/user/profilepicture/:id", UserController.changeProfilePicture);

  app.post("/conversation", ConversationController.newConversation);
  app.get("/conversations/:id", ConversationController.getConversations);

  // app.get("/dogs", async (_: Request, res: Response): Promise<Response> => {
  //   const allDogs: Dog[] = await Dog.findAll();
  //   return res.status(200).json(allDogs);
  // });

  // app.get(
  //   "/dogs/:id",
  //   async (req: Request, res: Response): Promise<Response> => {
  //     const { id } = req.params;
  //     const dog: Dog | null = await Dog.findByPk(id);
  //     return res.status(200).json(dog);
  //   }
  // );

  // app.post("/dogs", async (req: Request, res: Response): Promise<Response> => {
  //   const dog: Dog = await Dog.create({ ...req.body });
  //   return res.status(201).json(dog);
  // });

  // app.put(
  //   "/dogs/:id",
  //   async (req: Request, res: Response): Promise<Response> => {
  //     const { id } = req.params;
  //     await Dog.update({ ...req.body }, { where: { id } });
  //     const updatedDog: Dog | null = await Dog.findByPk(id);
  //     return res.status(200).json(updatedDog);
  //   }
  // );

  // app.delete(
  //   "/dogs/:id",
  //   async (req: Request, res: Response): Promise<Response> => {
  //     const { id } = req.params;
  //     const deletedDog: Dog | null = await Dog.findByPk(id);
  //     await Dog.destroy({ where: { id } });
  //     return res.status(200).json(deletedDog);
  //   }
  // );
};
