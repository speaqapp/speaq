import { Sequelize } from "sequelize-typescript";
import { Conversation } from "../models/Conversation";
import { ConversationUser } from "../models/ConversationUser";
import { Message } from "../models/Message";
import { User } from "../models/User";

export const connection = new Sequelize({
  dialect: "postgres",
  host: "localhost",
  username: "speaq",
  password: "speaq",
  database: "speaq",
  logging: false,
  models: [User, ConversationUser, Conversation, Message],
});
