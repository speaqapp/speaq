const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const morgan = require("morgan");

const { sequelize } = require("./models");

const config = require("./config/config");

const app = express();

app.use(cors());
app.use(helmet());
app.options("*", cors());
app.use(morgan("tiny"));
app.use(express.json());

require("./routes")(app);

sequelize.sync({ force: false }).then(() => {
  app.listen(config.port);
  console.log(`Server started on port ${config.port}`);
});
