module.exports = {
  port: process.env.PORT || 8081,
  db: {
    database: process.env.DB_NAME || "speaq",
    user: process.env.DB_USER || "speaq",
    password: process.env.DB_PASS || "speaq",
    options: {
      dialect: process.env.DIALECT || "postgres",
      host: process.env.HOST || "localhost",
    },
  },
  authentication: {
    jwtSecret: process.env.JWT_SECRET || "secret"
  }
};
