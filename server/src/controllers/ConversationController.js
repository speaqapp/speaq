const { Conversation } = require("../models");
const { Op } = require("sequelize");

module.exports = {
  //new conversation
  async newConversation(req, res) {
    const newConversation = new Conversation({
      members: [req.body.senderId, req.body.receiverId],
    });

    try {
      const savedConversation = await newConversation.save();
      res.status(200).json(savedConversation);
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //get user conversations
  async getConversations(req, res) {
    try {
      const conversations = await Conversation.findAll({
        where: { members: { [Op.contains]: [req.params.userId] } },
      });

      res.status(200).json(conversations);
      // res.status(200).message('hey')
    } catch (err) {
      res.status(500).json(err);
    }
  },
  
  // get conversation with two userId
  async findConversation(req, res) {
    try {
      const conversation = await Conversation.findOne({
        where: {
          [Op.or]: [
            {
              members: [req.params.firstUserId, req.params.secondUserId]
            },
            {
              members: [req.params.secondUserId, req.params.firstUserId]
            }
          ]
        },
      });
      res.status(200).json(conversation);
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
