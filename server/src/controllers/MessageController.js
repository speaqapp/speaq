const { Message } = require("../models");

module.exports = {
  async newMessage(req, res) {
    const newMessage = new Message(req.body);

    try {
      const savedMessage = await newMessage.save();
      res.status(200).json(savedMessage);
    } catch (err) {
      res.status(500).json(err);
    }
  },

  async getMessages(req, res) {
    try {
      const messages = await Message.findAll({
        where: {
          conversationId: req.params.conversationId,
        },
      });
      res.status(200).json(messages);
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
