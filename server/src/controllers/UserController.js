const { User } = require("../models");
const sequelize = require("sequelize");

module.exports = {
  async changeProfilePicture(req, res) {
    try {
      const user = await User.update(
        { profilepicture: req.body.profilepicture },
        { where: { id: req.params.userId } }
      );
      res.status(200).json(user);
    } catch (err) {
      res.status(500).json(err);
    }
  },
  async getUser(req, res) {
    try {
      const user = await User.findOne({ where: { id: req.params.userId } });
      const userData = {
        username: user.username,
        profilepicture: user.profilepicture,
      };
      res.status(200).json(userData);
    } catch (err) {
      res.status(500).json(err);
    }
  },
  async addFriend(req, res) {
    try {
      const { userId, friendUsername } = req.body;

      const friend = await User.findOne({
        where: { username: friendUsername },
      });

      await User.update(
        {
          friends: sequelize.fn(
            "array_append",
            sequelize.col("friends"),
            friend.id
          ),
        },
        { where: { id: userId } }
      );

      res.status(200).json();
    } catch (err) {
      res.status(500).json(err);
    }
  },
  async getFriends(req, res) {
    try {
      const user = await User.findOne({ where: { id: req.params.userId } });
      const friends = user.friends;
      res.status(200).json(friends);
    } catch (err) {
      res.status(500).json(err);
    }
  },
};
