module.exports = (sequelize, DataTypes) => {
  const Conversation = sequelize.define(
    "Conversation",
    {
      members: {
        type: DataTypes.ARRAY(DataTypes.TEXT),
      },
    },
  );
  
  return Conversation;
};
