module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define("Message", {
    conversationId: {
      type: DataTypes.STRING,
    },
    sender: {
      type: DataTypes.STRING,
    },
    text: {
      type: DataTypes.STRING,
    },
  });

  return Message;
};
