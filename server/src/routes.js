const AuthController = require("./controllers/AuthController");
const ConversationController = require("./controllers/ConversationController");
const MessageController = require("./controllers/MessageController");
const UserController = require("./controllers/UserController");
const AuthControllerPolicy = require("./policies/AuthControllerPolicy");

module.exports = (app) => {
  // AUTH
  app.post("/register", AuthControllerPolicy.register, AuthController.register);
  app.post("/login", AuthController.login);

  // USER
  app.post("/user/profilepicture/:userId", UserController.changeProfilePicture);
  app.post("/user/addFriend/", UserController.addFriend);
  app.get("/user/:userId", UserController.getUser);
  app.get("/user/friends/:userId", UserController.getFriends);

  // CONVERSATION
  app.post("/conversation", ConversationController.newConversation);
  app.get("/conversations/:userId", ConversationController.getConversations);
  app.get(
    "/conversation/find/:firstUserId/:secondUserId",
    ConversationController.findConversation
  );

  // MESSAGE
  app.post("/message", MessageController.newMessage);
  app.get("/message/:conversationId", MessageController.getMessages);
};
