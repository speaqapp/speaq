import { Socket } from "socket.io";

interface User {
  userId: number;
  socketId: string;
}

const whitelist = ["http://localhost:8080", "http://localhost:8082"];
const corsOptions = {
  origin: function (origin: any, callback: any) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};

const io = require("socket.io")(8900, {
  cors: corsOptions,
});

let users = [] as User[];

const addUser = (userId: number, socketId: string) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
};

const removeUser = (socketId: string) => {
  users = users.filter((user) => user.socketId !== socketId);
};

io.on("connection", (socket: Socket) => {
  //when connected
  console.log("a user connected.");

  //take userId and socketId from user, add to user array
  socket.on("addUser", (userId) => {
    addUser(userId, socket.id);
    io.emit("getUsers", users);
    console.log(`added user: ${userId} -> ${socket.id}`);
  });

  //send and get message
  socket.on("sendMessage", ({ senderId, receiverId, text }) => {
    const user = users.find((user) => {
      return user.userId == receiverId;
    });
    console.log(user);
    
    console.log(`sent message to: ${user?.userId} -> ${text}`);
    io.to(user?.socketId).emit("getMessage", {
      senderId,
      text,
    });
  });

  //when disconnected
  socket.on("disconnect", () => {
    console.log("a user disconnected!");
    removeUser(socket.id);
    io.emit("getUsers", users);
  });
});
